Adempas Riociguat
http://www.adempas-us.com/hcp/

***************************************************************************

Prescribing Information
http://labeling.bayerhealthcare.com/html/products/pi/Adempas_PI.pdf

Medication Guide
http://labeling.bayerhealthcare.com/html/products/pi/Adempas_MG.pdf

IMPORTANT SAFETY INFORMATION
WARNING: EMBRYO-FETAL TOXICITY. Do not administer Adempas to a pregnant female because it may cause fetal harm. Females of reproductive potential: Exclude pregnancy before the start of treatment, monthly during treatment, and 1 month after stopping treatment. Prevent pregnancy during treatment and for one month after stopping treatment by using acceptable methods of contraception. For all female patients, Adempas is available only through a restricted program called the Adempas Risk Evaluation and Mitigation Strategy (REMS) Program... Continue reading below.

***************************************************************************

Bayer
http://www.bayer.com

***************************************************************************

Discover Our Brand New Site, Redesigned For You

Our new site is designed to help you learn what types of patients could benefit from Adempas. 

You’ll be able to get important information for each patient type to help you decide if Adempas is right for your patient.

Just click on a patient profile to discover how Adempas could help treat adults with pulmonary arterial hypertension (PAH) (WHO Group 1) or inoperable or persistent/recurrent chronic thromboembolic pulmonary hypertension (CTEPH) (WHO Group 4) after surgery.

We hope the new site creates a positive experience for you and your patients. 

***************************************************************************

Indications

1) Adempas (riociguat) tablets are indicated for the treatment of adults with persistent/recurrent chronic thromboembolic pulmonary hypertension (CTEPH), (WHO Group 4) after surgical treatment, or inoperable CTEPH, to improve exercise capacity and WHO functional class.

2) Adempas is indicated for the treatment of adults with pulmonary arterial hypertension (PAH), (WHO Group 1), to improve exercise capacity, WHO functional class and to delay clinical worsening.*

Efficacy was shown in patients on Adempas monotherapy or in combination with endothelin receptor antagonists or prostanoids. Studies establishing effectiveness included predominantly patients with WHO functional class II–III and etiologies of idiopathic or heritable PAH (61%) or PAH associated with connective tissue diseases (25%).

*Time to clinical worsening was a combined endpoint defined as death (all-cause mortality), heart/lung transplantation, atrial septostomy, hospitalization due to persistent worsening of pulmonary hypertension, start of new PAH-specific treatment, persistent decrease in 6MWD and persistent worsening of WHO functional class.

***************************************************************************

IMPORTANT SAFETY INFORMATION

WARNING: EMBRYO-FETAL TOXICITY 	

Do not administer Adempas (riociguat) tablets to a pregnant female because it may cause fetal harm.
	
Females of reproductive potential:  Exclude pregnancy before the start of treatment, monthly during treatment, and 1 month after stopping treatment.  Prevent pregnancy during treatment and for one month after stopping treatment by using acceptable methods of contraception.

For all female patients, Adempas is available only through a restricted program called the Adempas Risk Evaluation and Mitigation Strategy (REMS) Program.


***************************************************************************

Contraindications

Adempas is contraindicated in:

1) Pregnancy. Adempas may cause fetal harm when administered to a pregnant woman. Adempas was consistently shown to have teratogenic effects when administered to animals. If this drug is used during pregnancy, or if the patient becomes pregnant while taking this drug, the patient should be apprised of the potential hazard to the fetus 	

2) Co-administration with nitrates or nitric oxide donors (such as amyl nitrite) in any form. 	

3) Concomitant administration with specific phosphodiesterase-5 (PDE-5) inhibitors (such as sildenafil, tadalafil, or vardenafil) or nonspecific PDE inhibitors (such as dipyridamole or theophylline).

***************************************************************************

Warnings and Precautions

Embryo-Fetal Toxicity. Adempas may cause fetal harm when administered during pregnancy and is contraindicated for use in women who are pregnant. In females of reproductive potential, exclude pregnancy prior to initiation of therapy, advise use of acceptable contraception and obtain monthly pregnancy tests. For females, Adempas is only available through a restricted program under the Adempas REMS Program. 

Adempas REMS Program. Females can only receive Adempas through the Adempas REMS Program, a restricted distribution program. 	

Important requirements of the Adempas REMS program include the following:

1) Prescribers must be certified with the program by enrolling and completing training.

2) All females, regardless of reproductive potential, must enroll in the Adempas REMS Program prior to initiating Adempas. Male patients are not enrolled in the Adempas REMS Program.	
	
3) Female patients of reproductive potential must comply with the pregnancy testing and contraception requirements. 	
	
4) Pharmacies must be certified with the program and must only dispense to patients who are authorized to receive Adempas. 	

Further information, including a list of certified pharmacies, is available at www.AdempasREMS.com or 1-855-4ADEMPAS. 
https://www.adempasrems.com/#MainContent/!Home

Hypotension. Adempas reduces blood pressure. Consider the potential for symptomatic hypotension or ischemia in patients with hypovolemia, severe left ventricular outflow obstruction, resting hypotension, autonomic dysfunction, or concomitant treatment with antihypertensives or strong CYP and P-gp/BCRP inhibitors. Consider a dose reduction if patient develops signs or symptoms of hypotension. 

Bleeding. In the placebo-controlled clinical trials, serious bleeding occurred in 2.4% of patients taking Adempas compared to 0% of placebo patients. Serious hemoptysis occurred in 5 (1%) patients taking Adempas compared to 0 placebo patients, including one event with fatal outcome. Serious hemorrhagic events also included 2 patients with vaginal hemorrhage, 2 with catheter site hemorrhage, and 1 each with subdural hematoma, hematemesis, and intra-abdominal hemorrhage.  	

Pulmonary Veno-Occlusive Disease. Pulmonary vasodilators may significantly worsen the cardiovascular status of patients with pulmonary veno-occlusive disease (PVOD). Therefore, administration of Adempas to such patients is not recommended. Should signs of pulmonary edema occur, the possibility of associated PVOD should be considered and if confirmed, discontinue treatment with Adempas. 

***************************************************************************

Most Common Adverse Reactions

The most common adverse reactions occurring more frequently (≥3%) on Adempas than placebo were headache (27% vs 18%), dyspepsia/gastritis (21% vs. 8%), dizziness (20% vs 13%), nausea (14% vs 11%), diarrhea (12% vs 8%), hypotension (10% vs 4%), vomiting (10% vs 7%), anemia (7% vs 2%), gastroesophageal reflux disease (5% vs 2%), and constipation (5% vs 1%).

Other events that were seen more frequently in Adempas compared to placebo and potentially related to treatment were: palpitations, nasal congestion, epistaxis, dysphagia, abdominal distension and peripheral edema.

For important risk and use information, please see the full, including Boxed Warning.
http://labeling.bayerhealthcare.com/html/products/pi/Adempas_PI.pdf

To find out more about how Adempas can help your patients, visit Adempas-US.com. 
http://www.adempas-us.com/index.php

***************************************************************************

You are encouraged to report negative side effects or quality complaints of prescription drugs to the FDA. Visit www.fda.gov/medwatch, or call 1-800-FDA-1088.

BAYER, the Bayer Cross and Adempas are registered trademarks of Bayer. All other trademarks are property of their respective owners. These trademark owners are not affiliated with Bayer and do not sponsor or endorse Bayer or this product.
http://www.fda.gov/Safety/MedWatch/default.htm

Bayer HealthCare LLC   100 Bayer Boulevard   Whippany, NJ 07981

Prescribing Information
http://labeling.bayerhealthcare.com/html/products/pi/Adempas_PI.pdf

Información de Prescripción
http://labeling.bayerhealthcare.com/html/products/pi/AdempasSP_PI.pdf

Privacy Policy
http://labeling.bayerhealthcare.com/html/privacy.htm

Terms of Use
http://labeling.bayerhealthcare.com/html/terms.htm

Contact Us
http://www.adempas-us.com/contact/

(c) 2014 Bayer HealthCare Pharmaceuticals. All rights reserved.

400-209-0003-14g July 2014